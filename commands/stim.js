module.exports = {
    name: 'stim',
    description: 'Cancel the tranquilization or coma effect of a user.',
    async execute(message, args, tranqUsers, comaUsers) {
        try {
            // Define role IDs for authorized users
            const authorizedRoles = ['969767633278869515', '827303828884946944', '827332588803850270'];

            // Check if the command user has the authorized role
            const userRoles = message.member.roles.cache.map(role => role.id);
            console.log('User roles:', userRoles); // Debugging line
            console.log('Authorized roles:', authorizedRoles); // Debugging line

            if (!userRoles.some(role => authorizedRoles.includes(role))) {
                return message.reply('You do not have permission to use this command.');
            }

            // Identify the target user
            let target;
            if (message.mentions.members.first()) {
                target = message.mentions.members.first();
            } else if (message.reference) {
                target = await message.channel.messages.fetch(message.reference.messageId).then(msg => msg.member);
            } else {
                return message.reply('Please mention a user or reply to their message to use this command.');
            }

            // Ensure comaUsers and tranqUsers are initialized
            if (!tranqUsers || !comaUsers) {
                return message.reply('The user sets are not properly initialized.');
            }

            // Initialize confirmation message
            let confirmationMessage = '';

            // Debugging lines to check user status
            console.log(`Checking status of user ${target.id}`);
            console.log(`Is user in comaUsers: ${comaUsers.has(target.id)}`);
            console.log(`Is user in tranqUsers: ${tranqUsers.has(target.id)}`);

            // Remove user from both sets
            const wasComatose = comaUsers.delete(target.id);
            const wasTranquilized = tranqUsers.delete(target.id);

            if (wasComatose || wasTranquilized) {
                confirmationMessage = `${target.displayName} has been stimmed and is no longer tranquilized or in a coma.`;
                console.log(`Removed ${target.id} from comaUsers or tranqUsers`); // Debugging line
            } else {
                return message.reply('The user is not currently tranquilized or in a coma.');
            }

            // Send the appropriate confirmation message
            message.channel.send(confirmationMessage);

        } catch (error) {
            console.error('An error occurred:', error);
            message.channel.send('There was an error trying to execute the command.');
        }
    },
};
