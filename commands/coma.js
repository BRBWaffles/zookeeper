module.exports = {
    name: 'coma',
    description: 'Temporarily delete all messages and reactions of a user for 24 hours.',
    async execute(message, args, comaUsers) {
        try {
            // Define role IDs
            const authorizedRoles = ['969767633278869515', '827303828884946944', '827332588803850270'];
            const targetRoleID = '1160622954082738347';

            // Check if the command user has the authorized role
            const userRoles = message.member.roles.cache.map(role => role.id);
            console.log('User roles:', userRoles); // Debugging line
            console.log('Authorized roles:', authorizedRoles); // Debugging line

            if (!userRoles.some(role => authorizedRoles.includes(role))) {
                return message.reply('You do not have permission to use this command.');
            }

            // Identify the target user
            let target;
            if (message.mentions.members.first()) {
                target = message.mentions.members.first();
            } else if (message.reference) {
                target = await message.channel.messages.fetch(message.reference.messageId).then(msg => msg.member);
            } else {
                return message.reply('Please mention a user or reply to their message to use this command.');
            }

            // Check if the target user has the specific role
            if (!target.roles.cache.has(targetRoleID)) {
                return message.reply('The target user does not have the required role.');
            }

            // Define the time period to delete messages
            const timePeriod = 24 * 60 * 60 * 1000; // 24 hours in milliseconds
            const now = Date.now();

            // Fetch the target user's recent messages and delete them
            const fetchOptions = {
                limit: 100, // Fetch up to 100 messages per channel
            };

            // Function to delete messages in a channel
            const deleteMessages = async (channel, userID) => {
                const messages = await channel.messages.fetch(fetchOptions);
                const userMessages = messages.filter(m => m.author.id === userID && (now - m.createdTimestamp) <= timePeriod);

                for (const msg of userMessages.values()) {
                    await msg.delete();
                }
            };

            // Function to remove reactions from messages in a channel
            const removeReactions = async (channel, userID) => {
                const messages = await channel.messages.fetch(fetchOptions);
                const userMessages = messages.filter(m => m.author.id === userID && (now - m.createdTimestamp) <= timePeriod);

                for (const msg of userMessages.values()) {
                    for (const reaction of msg.reactions.cache.values()) {
                        await reaction.users.remove(userID);
                    }
                }
            };

            // Iterate through all channels and delete the target user's messages and reactions
            for (const channel of message.guild.channels.cache.values()) {
                if (channel.type === 'GUILD_TEXT') {
                    await deleteMessages(channel, target.id);
                    await removeReactions(channel, target.id);
                }
            }

            // Add the user to the comaUsers set
            comaUsers.add(target.id);
            console.log(`Added ${target.id} to comaUsers`); // Debugging line
            console.log(`Current comaUsers: ${Array.from(comaUsers)}`); // Debugging line

            // Schedule removal from the comaUsers set after 24 hours
            setTimeout(() => {
                comaUsers.delete(target.id);
                message.channel.send(`${target.displayName} is no longer comatose.`);
            }, timePeriod);

            // Send a confirmation message
            message.channel.send(`${target.displayName} has received an overdose of tranquilizers and will be in a coma for 24 hours.`);

        } catch (error) {
            console.error('An error occurred:', error);
            message.channel.send('There was an error trying to execute the command.');
        }
    },
};
