#!/bin/bash

# Absolute path to your bot's main JavaScript file
BOT_JS_PATH="/home/nick/discord-bots/zookeeper/index.js"

# Log file for the bot
LOG_FILE="/home/nick/discord-bots/zookeeper/scripts/bot.log"

# Function to start the bot using forever
start_bot() {
    echo "$(date): Starting bot..." | tee -a $LOG_FILE
    forever start --spinSleepTime 5000 -c "node" $BOT_JS_PATH >> $LOG_FILE 2>&1
}

# Function to stop the bot
stop_bot() {
    echo "$(date): Stopping bot..." | tee -a $LOG_FILE
    forever stop $BOT_JS_PATH >> $LOG_FILE 2>&1
}

# Check if the bot is already running
if forever list | grep -q $BOT_JS_PATH; then
    echo "$(date): Bot is already running. Restarting bot..." | tee -a $LOG_FILE
    stop_bot
fi

# Start the bot
start_bot

# Keep the script running to monitor the bot
while true; do
    sleep 1
    if ! forever list | grep -q $BOT_JS_PATH; then
        echo "$(date): Bot has crashed. Restarting bot..." | tee -a $LOG_FILE
        start_bot
    fi
done
